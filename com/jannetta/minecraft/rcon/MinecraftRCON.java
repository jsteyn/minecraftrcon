package com.jannetta.minecraft.rcon;

/**
 * @author jannetta
 * Remote console for Minecraft using RCON library
 * The program can be started with parameters for the ip address, port and password, 
 * or it can be set once the program is running.
 *
 */
public class MinecraftRCON {


	static RemoteConsole rcon = null;
	
	public static void main(String[] args) {
		// Connects to 127.0.0.1 on port 27015
		if (args.length != 3) {
			System.out.println("Syntax: java -jar MinecraftRCON [ipaddress] [port] [password]");
			rcon = RemoteConsole.getInstance();
		} else {
			rcon = RemoteConsole.getInstance(args);
		}
		
		MinecraftRCONFrame mr = new MinecraftRCONFrame("Minecraft Remote Console");
		mr.pack();
		mr.setSize(640,480);
		mr.setVisible(true);
	}
	
}
