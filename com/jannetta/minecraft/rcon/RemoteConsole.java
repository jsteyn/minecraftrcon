package com.jannetta.minecraft.rcon;

import java.io.IOException;
import net.kronos.rkon.core.Rcon;
import net.kronos.rkon.core.ex.AuthenticationException;
import net.kronos.rkon.core.ex.MalformedPacketException;

public class RemoteConsole {

	public static Rcon rcon;

	private static RemoteConsole instance = null;

	private static String ipaddress = null, port = null, password = null;
	
	private static boolean connected = false;

	protected RemoteConsole() {
		// Defeat instantiation
	}

	public static RemoteConsole getInstance() {
		if (instance == null) {
			instance = new RemoteConsole();
		} 
		return instance;
	}

	public static RemoteConsole getInstance(String[] args) {
		if (instance == null) {
			if (!(args[0] == null) || (args[1] == null) || (args[2] == null)) {
				try {
					ipaddress = args[0];
					port = args[1];
					password = args[2];
					rcon = new Rcon(args[0], Integer.valueOf(args[1]), args[2].getBytes());
					connected = true;
				} catch (MalformedPacketException e) {
					System.out.println(e.getMessage());
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (AuthenticationException e) {
					e.printStackTrace();
				}
			} else
				try {
					throw new ConnectionNotSetException("One of the connection parameters are missing");
				} catch (ConnectionNotSetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			instance = new RemoteConsole();
		}
		return instance;
	}

	public String sendCommand(String command) {
		try {
			return rcon.command(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void disconnect() {
		try {
			rcon.disconnect();
			connected = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean connect(String ip, String port, String password){
		try {
			rcon = new Rcon(ip, Integer.valueOf(port), password.getBytes());
			connected = true;
			return true;
		} catch (NumberFormatException | IOException | AuthenticationException e) {
			// TODO Auto-generated catch block
			String failmessage = e.getMessage();
			System.out.println(failmessage);
			connected = false;
			return false;
		}
	}
	
	public boolean isConnected() {
		return connected;
	}

	public static String getIpaddress() {
		return ipaddress;
	}

	public static void setIpaddress(String ipaddress) {
		RemoteConsole.ipaddress = ipaddress;
	}

	public static String getPort() {
		return port;
	}

	public static void setPort(String port) {
		RemoteConsole.port = port;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		RemoteConsole.password = password;
	}

}
