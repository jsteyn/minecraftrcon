package com.jannetta.minecraft.rcon;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class QuickCommandsJPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RemoteConsole rcon = RemoteConsole.getInstance();
	private JTextField user;
	private ConsoleJPanel console;
	private JTextField ip;
	private JTextField port;
	private JTextField password;
	private JButton connect;
	private JButton disconnect;
	private JPanel connection;
	private JPanel quick;
	private JButton list;
	private JButton help1;
	private JButton help2;
	private JButton help3;
	private JButton help4;
	private JButton help5;
	private JButton help6;
	private JButton help7;
	private JButton help8;
	private JButton help9;
	private JButton whitelistadd;
	private JButton whitelistremove;

	QuickCommandsJPanel(ConsoleJPanel console) {
		this.console = console;
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		setLayout(null);
		String deopbutton = "Deop User";
		JButton deop = new JButton(deopbutton);
		String opbutton = "Op User";
		JButton op = new JButton(opbutton);
		ip = new JTextField(RemoteConsole.getIpaddress());
		port = new JTextField(RemoteConsole.getPort());
		password = new JTextField(RemoteConsole.getPassword());
		connect = new JButton("Connect");
		disconnect = new JButton("Disconnect");
		user = new JTextField("username");
		list = new JButton("List users");
		help1 = new JButton("Help 1");
		help2 = new JButton("Help 2");
		help3 = new JButton("Help 3");
		help4 = new JButton("Help 4");
		help5 = new JButton("Help 5");
		help6 = new JButton("Help 6");
		help7 = new JButton("Help 7");
		help8 = new JButton("Help 8");
		help9 = new JButton("Help 9");
		whitelistadd = new JButton("Whitelist add");
		whitelistremove = new JButton("Whitelist remove");
		user.selectAll();

		connection = new JPanel(new FlowLayout());
		connection.setBorder(BorderFactory.createLineBorder(Color.red));
		connection.add(ip);
		connection.add(port);
		connection.add(password);
		connection.add(connect);
		connection.add(disconnect);

		quick = new JPanel(new FlowLayout());
		quick.setBorder(BorderFactory.createLineBorder(Color.green));
		quick.add(user);
		quick.add(deop);
		quick.add(op);
		quick.add(list);
		quick.add(help1);
		quick.add(help2);
		quick.add(help3);
		quick.add(help4);
		quick.add(help5);
		quick.add(help6);
		quick.add(help7);
		quick.add(help8);
		quick.add(help9);
		quick.add(whitelistadd);
		quick.add(whitelistremove);

		add(connection);
		add(quick);

		connect.addActionListener(this);
		disconnect.addActionListener(this);
		deop.addActionListener(this);
		op.addActionListener(this);
		list.addActionListener(this);
		help1.addActionListener(this);
		help2.addActionListener(this);
		help3.addActionListener(this);
		help4.addActionListener(this);
		help5.addActionListener(this);
		help6.addActionListener(this);
		help7.addActionListener(this);
		help8.addActionListener(this);
		help9.addActionListener(this);
		whitelistadd.addActionListener(this);
		whitelistremove.addActionListener(this);

		connection.setBounds(5, 5, 200, 120);
		quick.setBounds(5, 130, 200, 290);
		ip.setColumns(15);
		port.setColumns(15);
		password.setColumns(15);
		user.setColumns(15);
		if (rcon.isConnected()) {
			connect.setEnabled(false);
			disconnect.setEnabled(true);
		} else {
			connect.setEnabled(true);
			disconnect.setEnabled(false);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Connect")) {
			if (rcon.connect(ip.getText(), port.getText(), password.getText())) {
				connect.setEnabled(false);
				disconnect.setEnabled(true);
			}
			;
		}
		if (rcon.isConnected()) {
			if (e.getActionCommand().equals("Deop User")) {
				String command = "deop " + user.getText();
				String result = rcon.sendCommand(command);
				console.appendToPane(console.getPane(), result);
			}
			if (e.getActionCommand().equals("Op User")) {
				String command = "op " + user.getText();
				String result = rcon.sendCommand(command);
				console.appendToPane(console.getPane(), result);
			}
			if (e.getActionCommand().equals("Disconnect")) {
				rcon.disconnect();
				connect.setEnabled(true);
				disconnect.setEnabled(false);
			}
			if (e.getActionCommand().equals("List users")) {
				String result = rcon.sendCommand("list");
				console.appendToPane(console.getPane(), result);
			}
			if (e.getActionCommand().substring(0, 4).equals("Help")) {
				String result = rcon.sendCommand(e.getActionCommand());
				console.appendToPane(console.getPane(), result);
			}
			if (e.getActionCommand().substring(0, 9).equals("Whitelist")) {
				String result = rcon.sendCommand(e.getActionCommand() + " " + user.getText());
				console.appendToPane(console.getPane(), result);
			}
		} else {
			JOptionPane.showMessageDialog(this, "There is no connection to a server.");
		}

	}

}
