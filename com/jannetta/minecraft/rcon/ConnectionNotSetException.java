package com.jannetta.minecraft.rcon;

public class ConnectionNotSetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ConnectionNotSetException(String message) {
		super(message);
	}

}
