package com.jannetta.minecraft.rcon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleContext;
import javax.swing.text.StyleConstants;

public class ConsoleJPanel extends JPanel implements ActionListener {

	private JTextPane console = new JTextPane();
	private JTextField input = new JTextField("Enter commend here.");
	JScrollPane scrollconsole = new JScrollPane(console);
	RemoteConsole rcon = RemoteConsole.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	ConsoleJPanel() {
		setLayout(new BorderLayout());
		scrollconsole.setPreferredSize(new Dimension(200, 400));
		console.setBackground(Color.BLACK);
		console.setText("");
		input.addActionListener(this);
		input.selectAll();
		add(scrollconsole, BorderLayout.NORTH);
		add(input, BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String ret = rcon.sendCommand(e.getActionCommand()) + "\n";
		ret = "�4" + ret;
		appendToPane(console, ret);
	}

	public void appendToPane(JTextPane tp, String msg) {
		Color c = Color.white;
		String[] lines = msg.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			line = line.replaceAll("�", "��");
			String[] subline = line.split("�");
			for (int j = 0; j < subline.length; j++) {
				if (subline[j].length() == 0 || subline[j] == null) {

				} else {
					if (subline[j].startsWith("�0")) {
						c = Color.black;
					} else if (subline[j].startsWith("�4")) {
						c = Color.red;
					} else if (subline[j].startsWith("�6")) {
						c = Color.ORANGE;
					} else if (subline[j].startsWith("�7")) {
						c = Color.GRAY;
					} else if (subline[j].startsWith("�e")) {
						c = Color.yellow;
					} else if (subline[j].startsWith("�f")) {
						c = Color.white;
					} else {
						c = Color.white;
					}
					if (subline[j].startsWith("�"))
						subline[j] = subline[j].substring(2, subline[j].length()) + "\n";
					else
						subline[j] = subline[j] + "\n";
					StyleContext sc = StyleContext.getDefaultStyleContext();
					AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

					aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
					aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

					int len = tp.getDocument().getLength();
					tp.setCaretPosition(len);
					tp.setCharacterAttributes(aset, false);
					tp.replaceSelection(subline[j]);
				}
			}
		}
	}

	public JTextPane getPane() {
		return console;
	}
}
