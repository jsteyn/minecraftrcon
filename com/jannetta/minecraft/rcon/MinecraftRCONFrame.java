package com.jannetta.minecraft.rcon;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * @author jannetta Main JFrame for MinecraftRCON.
 *
 */
public class MinecraftRCONFrame extends JFrame implements ActionListener, WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ConsoleJPanel console = new ConsoleJPanel();
	QuickCommandsJPanel quickCommands = new QuickCommandsJPanel(console);
	private static RemoteConsole rcon = RemoteConsole.getInstance();
	JMenuBar mb = new JMenuBar();
	JMenu help_menu = new JMenu("Help");
	JMenuItem help_about = new JMenuItem("About");

	MinecraftRCONFrame(String title) {
		super(title);
		setLayout(new BorderLayout());
		setSize(650, 480);
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(this);

		setIconImage(new ImageIcon(Toolkit.getDefaultToolkit().getClass().getResource("/res/MineCraft.png")).getImage());
		help_menu.add(help_about);
		mb.add(help_menu);
		setJMenuBar(mb);
		help_about.addActionListener(this);

		quickCommands.setPreferredSize(new Dimension(300, 400));
		console.setPreferredSize(new Dimension(320, 400));
		getContentPane().add(quickCommands, BorderLayout.WEST);
		getContentPane().add(console, BorderLayout.EAST);
		pack();
		setResizable(false);
		setVisible(true);
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if (rcon.isConnected()) {
			RemoteConsole.getInstance().disconnect();
		}
		System.out.println("Console connection closed.");
		System.exit(0);

	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("About")) {
			// default title and icon
			JOptionPane.showMessageDialog(this,
					"MinecraftRCON is developed by Jannetta Steyn.\n" + "Copyright (c): Jannetta S Steyn\n"
							+ "Version 1.0\n"
							+ "MinecraftRCON should run on any operating system that runs Java.\n"
							+ "Please use the issue tracker at https://bitbucket.org/jsteyn/minecraftrcon\n"
							+ "if you experience any problems with the program.");
		}
	}

}
